/**
 * @author Floris Balm, s1205916
 * @version 0.1 2013-09-16
 * This class is the class that holds the actual grid and its implemented logic.
 */

public class PercolationGrid
{
    private int[][] grid;
    private final int gridDimension;
    public PercolationGrid(int gridSize)
    {
        gridDimension = gridSize +2;
        grid = new int[gridDimension][gridDimension];

    }
    /**
     * @param density This is the value to set the density with.
     * A lower number for density will correspond to a more filled array.
     */
    public void fillGrid(double density)
    {
        for(int i = 1; i<gridDimension-1; ++i){
            for (int j = 1; j<gridDimension-1; ++j){
                //generate a random fillValue between 0.0 and 1.0.
                //A lower density would get more fill, so if the fillValue
                //density is lower, fillValue>density will evaluate true more often
                double fillValue = Math.random();
                grid[i][j] = (fillValue>density)?1:0;
            }
        }
    }
    /**
     * Set the filled cells to a unique number each, starting with 1
     */
    public void initializeGrid()
    {
        int count = 1;
        for(int i = 1; i<gridDimension-1; ++i){
            for(int j = 1; j<gridDimension-1; ++j){
                if (grid[i][j] == 1){
                    grid[i][j] = count;
                    ++count;
                }
            }
        }
    }
    public int nextStep()
    {
        int changesMade = 0;
        for (int i = 1; i<gridDimension-1; ++i){

            for(int j = 1; j<gridDimension-1; ++j){
                int thisPosition = grid[i][j];
                if (thisPosition>0){

                    if(grid[i-1][j]>thisPosition || grid[i+1][j]>thisPosition||
                            grid[i][j-1]>thisPosition || grid[i][j+1]>thisPosition){

                                changesMade += 1;
                                grid[i][j]+=1;
                            }

                }
            }
        }
        return changesMade;
    }

    public void printGrid()
    {
        for(int[] row : grid){
            for (int i:row){
                System.out.printf("%4d ", i);
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    public boolean isPercolated()
    {
        int[] firstRow = new int[gridDimension-2];
        for (int i = 1; i<gridDimension-1; ++i){
            firstRow[i-1] = grid[1][i];
        }
        int[] lastRow = new int[gridDimension-2];
        for (int i = 1; i<gridDimension-1; ++i){
            lastRow[i-1]=grid[gridDimension-1][i];
        }
        for(int value : firstRow){
            for(int secondValue:lastRow){
                if (value==secondValue && value>0){
                    return true;
                }
            }
        }
        return false;
    }
}
