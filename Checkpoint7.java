/**
 * @author Floris Balm, s1205916
 * @version 0.1 2013-09-16
 */
import java.util.Scanner;
public class Checkpoint7
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the density for the grid");
        double density = keyboard.nextDouble();
        System.out.println("Enter the grid size");
        PercolationGrid grid = new PercolationGrid(keyboard.nextInt());
        grid.fillGrid(density);
        grid.initializeGrid();
        grid.printGrid();
        while(grid.nextStep()!=0){
            grid.printGrid();
            try{Thread.sleep(3000);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        System.out.println("The grid " + (grid.isPercolated()?"did":"did not") + " have a path from top to bottom.");
        System.exit(0);
    }
}
